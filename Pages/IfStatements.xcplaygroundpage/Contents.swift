import UIKit

// Latihan 1
let wakeUpMax : Float = 06.00
var wakeUpRana : Float = 05.00

if(wakeUpRana >= wakeUpMax){
    print("You will be late, Rana!")
}else{
    print("Way to go! Keep Spirit")
}

// Latihan 2
var bloomingInterval : Int = 200
var pouring : Int

switch bloomingInterval {
case 0..<30:
    print("First interval and start pouring")
case 30...90:
    print("Second interval, after then wait until thirty second")
case 90...180:
    print("Third interval, after then wait until twenty five second")
case 180...225:
    print("Resting interval and your coffee are ready to serve")
default:
    print("Over resting")
}
