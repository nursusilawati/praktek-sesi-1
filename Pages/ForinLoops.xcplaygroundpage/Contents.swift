//: [Previous](@previous)

import Foundation

// Latihan 1

let custNames = ["Anna", "Hendry", "Disa", "Wendy", "Ryan"]

for name in custNames {
    print("Selamat datang di Kopi Surgawi, \(name)")
}

// Latihan 2
// Menggunakan Dictionary
let custOrders = ["Anna": "Vanilla Latte", "Hendry": "Cold White", "Disa": "Ice Americano", "Wendy": "Ice Japanese", "Ryan": "Ice Coffee Surgawi"]

for (name, order) in custOrders {
    print("Pesanan \(order) atas nama kak \(name) sudah ready!")
}

// Menggunakan Stuct
struct CustOrder {
    var name: String
    var order: String
}

let custOrder = [
    CustOrder(name: "Anna", order: "Vanilla Latte"),
    CustOrder(name: "Hendry", order: "Cold White"),
    CustOrder(name: "Disa", order: "Ice Americano"),
    CustOrder(name: "Wendy", order: "Ice Japanese"),
    CustOrder(name: "Ryan", order: "Ice Coffee Surgawi")
]

for orders in custOrder {
    print("Pesanan \(orders.order) atas nama kak \(orders.name) sudah ready!")
}

