//: [Previous](@previous)

import Foundation

//: [Next](@next)


// Latihan 1
var x : Int
let y : Int = 30
x = (y + 10)
print(x)

// Latihan 2 No 1
let content : String
let greeting : String = "Selamat Datang "
let palce : String = "Kopi Surgawi"
content = greeting + "di " + palce
print(content)

// Latihan 2 No 2
// cara 1
print("Jumlah karakter variabel \(content.count)")

//cara 2
print("Jumlah karakter variabel", content.count)

//cara 3
let count = content.count
print("Jumlah Karakter Variabel", count)

// Latihan 2 No 3
print(content.uppercased())

// Latihan 2 No 4
print(content.lowercased())

// Latihan 3
let coffeePrice : Int = 25000
var amountOfCoffee : Int = 2
var totalPrice : Int = (coffeePrice * amountOfCoffee)
var billMessage : String = ("Total price you should pay is IDR \(totalPrice)")
print(billMessage)


